import './App.css';
import React, { Component } from 'react'
import {Provider} from 'react-redux'
import store from './store'
import Director from './components/directorList'
import AddDir from './components/AddDirector'
import EditDirector from './components/EditDirector'
import DirectorInfo from './components/DirectorInfo'
import { BrowserRouter as Router, Switch, Route} from 'react-router-dom';



export class App extends Component {
  render() {
    return (
    <Provider store={store}>
     <Router>
       <Switch>
       <Route path ='/' exact component ={Director}/>
       <Route path ='/addDirector' exact component ={AddDir}/>
       <Route path ='/DirectorInfo/:id' exact component ={DirectorInfo}/>
       <Route path ='/EditDirector/:id' exact component ={EditDirector}/>
        </Switch>
      </Router>
    </Provider>
    )
  }
}

export default App


