export const fetchData = () => ({
    type: 'GETALL' 
})


export const addDirector = (dir) => ({
    type: 'ADD_DIR' ,
    dir
})

export const getDirector = (rank) => ({
    type: 'GET_DIR' ,
    rank
})


export const delDirector = (rank) => ({
    type: 'DEL_DIR' ,
    rank
})

export const editDirector = (id, dir) => ({
    type: 'EDIT_DIR' ,
    id:id,
    dir:dir
})