import React, { Component } from 'react'
import { connect } from 'react-redux'
import  { Link } from 'react-router-dom';
import {addDirector} from '../action/action'



export class AddDirector extends Component {

//   changeHandler=(e) => {
//         director.name = e.target.value;
//     }

    onSubmit = (e) =>{
        e.preventDefault(); 
        let director = {
            director : e.target[0].value
        }
        this.props.addDirector(director)
        this.props.history.push("./")
    }

    render() {
        return (
            <div className='addDirectorContainer'>
            <header className='navbar'>Add_New_Director</header>
            <form onSubmit={this.onSubmit} className='moviesForm'>

            <div className ='movie-info dir-Edit'>
                <div className='singleMoviesBox'>
                <label>Director :</label>
                <input className='input-dir'
                name="director"
                // value={director.name}
                placeholder ='Add director'
                // onChange ={this.changeHandler}
                autoFocus
                >
                </input>
                </div>
                </div>
                <div>
                <button type='submit' className='btn small-btn'  >+Add </button>

                <Link to='/'>
                    <button className='btn small-btn'  >Back </button>
                </Link>
                </div>
                
            </form>
            
        </div>
        )
    }
}

const mapStateToProps = (state) => ({
    // console.log(state)
})

const mapDispatchToProps = {
    addDirector
}

export default connect(mapStateToProps, mapDispatchToProps)(AddDirector)
