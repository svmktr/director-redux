import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom';
import {delDirector, getDirector} from '../action/action'
// window.location.reload();

export class DirectorInfo extends Component {


    componentDidMount = async() =>{
    const id = this.props.match.params.id
    await this.props.getDirector(id)  
}

    delete = (id) => {
        this.props.delDirector(id)
        this.props.history.push("/")
 
    };
 


    render() {
        // console.log(this.props.dirName)
        const id = this.props.match.params.id
        return (
           
            <div>
                <header className='navbar'>Dir_Info</header>
                <div className='movie-info-block info-block'>
                <ul className='dir-info'>
                    <li>Name: {this.props.dirName.director}</li>
                </ul>
                <Link to='/'>
                    <button className='btn small-btn' >Back</button>
                </Link>
                
                <button className='btn small-btn' onClick={() => this.delete(id)}>Delete</button>

                <Link to={`/EditDirector/${id}`}>
                    <button className='btn small-btn' >Edit</button>
                </Link>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    dirName : state.directors.director
})

const mapDispatchToProps = {
    delDirector,
    getDirector
}

export default connect(mapStateToProps, mapDispatchToProps)(DirectorInfo)
