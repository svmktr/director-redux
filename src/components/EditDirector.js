import React, { Component } from 'react'
import { connect } from 'react-redux'
import { getDirector, editDirector } from '../action/action'
import { Link } from 'react-router-dom';


class EditDirector extends Component {


    componentDidMount = async () => {
        const id = this.props.match.params.id
        // await(3000)
        // console.log(id)
        this.props.getDirector(id)
        await setTimeout(function(){
            
        }, 2000);
        // setTimeout(()=>{this.props.getDirector(id)}, 2000)
        // setTimeout(()=>{this.state.director=this.props.dirName.director},2000)
        // this.state.director=this.props.dirName.director
        // console.log(this.props.dirName)
    }

    state = {
        id: this.props.dirName.id,
        director: this.props.dirName.director
    }


    changeHandler = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }


    onSubmit = (e) => {
        e.preventDefault();
        // console.log(this.state.id);
        this.props.editDirector(this.state.id, this.state.director)
   
        this.props.history.push(`/DirectorInfo/${this.state.id}`)

        // console.log(e.target[0].value)   
    }

    render() {
        // this.state.director = this.props.dirName.director
        // let dir = this.props.dirName.director
        // console.log(this.props.dirName.id)
        return (
            <div className='addDirectorContainer'>
                <header className='navbar'>Edit_Director</header>
                <form onSubmit={this.onSubmit}>
                    <div className='movie-info dir-Edit'>
                        <label>Director :</label>
                        <input className='input-dir'
                            name="director"
                            type="text"
                            value={this.state.director}
                            onChange={this.changeHandler}
                            autoFocus
                        ></input>
                    </div>
                    <button type='submit' className='btn small-btn' >Update </button>
                    <Link to={`/DirectorInfo/${this.state.id}`}>
                        <button type='submit' className='btn small-btn' >Back </button>
                    </Link>
                </form>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    dirName: state.directors.director
})

const mapDispatchToProps = {
    getDirector,
    editDirector
}

export default connect(mapStateToProps, mapDispatchToProps)(EditDirector)
