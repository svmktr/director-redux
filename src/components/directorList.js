import React, { Component } from 'react'
import { connect } from 'react-redux'
import { fetchData } from "../action/action"
import { Link } from "react-router-dom";
// window.location.reload();

export class directorList extends Component {
    componentDidMount() {
        this.props.fetchData()
    }

    render() {
        // console.log(this.props.directors)

        return (
            <div className="Director-page">
            <header className="navbar">Director</header>

            <div className='section'>

            <Link to='/addDirector'>
              <button className="btn add-btn">+ Director</button>
            </Link>
  
            <ul className="list">
              {this.props.directors.map(item => (
                <Link to={`/DirectorInfo/${item.id}`} key={item.id}>
                  <li className='title' >{item.director}</li>      
                </Link>
              ))}
            </ul>
  
            </div>
          </div>
       

        )
    }
}

const mapStateToProps = (state) => ({
    directors: state.directors.directors
})

const mapDispatchToProps = {
    fetchData
}

export default connect(mapStateToProps, mapDispatchToProps)(directorList)
