const initialState = {
directors:[], 
director:''
}

export default (state = initialState, { type, payload }) => {
    switch (type) {

    case 'fetchedData':
        // console.log('reducer',payload)
        return { ...state,
            directors:payload
         }

    case 'getDirector':
        // console.log('reducer',payload)
    return { ...state,
        director:payload
        }


    default:
        return state
    }
}
