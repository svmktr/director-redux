import { combineReducers } from 'redux';
import directorReducer from './directorReducer'

export default combineReducers({
    directors: directorReducer
})