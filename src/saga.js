import { takeLatest, all, fork, put } from 'redux-saga/effects'

function* getAllDirectors() {
    const url = 'http://localhost:8000/api/directors'
    const json = yield fetch(url).then(
        res => res.json()
    )
    yield put({ type: 'fetchedData', payload: json })
}

function* actionWatcher() {
    yield takeLatest('GETALL', getAllDirectors)
}





function* addDirector(director) {
    yield fetch('http://localhost:8000/api/directors/', {
        method: 'POST',
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
        },
        body: JSON.stringify(director.dir),
    })
}

function* actionWatcher1() {
    yield takeLatest('ADD_DIR', addDirector)
}





function* getDirector(data) {
    // console.log(data.rank)
    const id = data.rank
    const url = `http://localhost:8000/api/directors/${id}`;
    const json = yield fetch(url)
        .then(res => res.json())
    yield put({ type: 'getDirector', payload: json })
}

function* actionWatcher2() {
    yield takeLatest('GET_DIR', getDirector)
}



function* deleteDirector(data) {
    const id = data.rank
    yield fetch((`http://localhost:8000/api/directors/${id}`), {
        method: 'delete'
    })
    // yield getDirector(id)
}

function* actionWatcher3() {
    yield takeLatest('DEL_DIR', deleteDirector)
}



function* editDirector(data) {
    // console.log(data.dir)
    const body = { director: data.dir }
    yield fetch(('http://localhost:8000/api/directors/' + data.id), {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
        },
        mode: 'cors',
        body: JSON.stringify(body)
    })
    // yield getDirector(data.id)
    // yield put({type: 'editDirector', payload: json})
    // yield getDirector(data.id)
}

function* actionWatcher4() {
    // console.log('Action Watcher')
    yield takeLatest("EDIT_DIR", editDirector)
}




export default function* rootSaga() {
    yield all([
        fork(actionWatcher),
        fork(actionWatcher1),
        fork(actionWatcher2),
        fork(actionWatcher3),
        fork(actionWatcher4)
    ])
}

// export default function* rootSaga(){
//     console.log('rootSaga')
// }
